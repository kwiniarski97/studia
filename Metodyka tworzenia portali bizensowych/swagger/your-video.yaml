openapi: 3.0.0
# Added by API Auto Mocking Plugin
servers:
  - description: SwaggerHub API Auto Mocking
    url: https://virtserver.swaggerhub.com/kwiniarski97/your-video/1.0.0
info:
  description: This is API for the the YourVideo™ website.
  version: "1.0.0"
  title: YourVideo™ API
  contact:
    email: admin@YourVideo.com
  license:
    name: Apache 2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
tags:
  - name: video
    description: Operations on videos
  - name: auth
    description: Operations related to user authorization
  - name: user
    description: User calls
  - name: admin
    description: Admin only calls
paths:
  /video/search:
    get:
      tags: 
       - video
      summary: Search for a video
      operationId: searchForVideo
      description: 
        Allows you to search for video, Paginated.
        All of the users have access to this endpoint. Not protected.
        For all users except administators it returns only pulic videos.
      parameters:
        - in: query
          name: query
          description: Query video will be searched with.
          required: true
          schema:
            type: string
        - in: query
          name: sort
          description: Sorting name
          required: true
          schema:
            $ref: '#/components/schemas/VideoSorting'
        - in: query
          name: direction
          description: Sorting name
          required: true
          schema:
            $ref: '#/components/schemas/SortingDirections'
      responses:
        '200':
          description: Items found.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/PaginatedVideos'
        '400':
          description: 'Error'
  /video:      
    post:
      tags:
        - video
      summary: Adds a new video
      operationId: addInventory
      description: Adds a new video the system. Only authenticated users have access to this endpoint.
      parameters:
        - in: header
          name: token
          description: JWT Auth token.
          required: true
          schema:
            type: string
      responses:
        '201':
          description: Item created returns ID of new item
          content:
            text/plain:
              schema:
                type: string
                example: 7a291aa7-d428-4e76-82ea-1f8d82994c79
        '400':
          description: 'invalid input, object invalid'
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/VideoAdd'
        description: Inventory item to add
  /video/{uuid}:
    parameters:
      - in: path
        name: uuid
        description: uuid of video
        required: true
        schema:
          type: uuid
    get:
      tags:
        - video
      summary: Returns single video
      operationId: getVideo
      description: |
        By passing proper video id you can get video you have access to. When video is not public then only owner and admininistrator can view it.
      responses:
        '200':
          description: search results matching criteria
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Video'
        '403':
          description: Unauthorized
        '422':
          description: 'Entity not found'
    put:
      tags:
        - video
      summary: Updates a video
      operationId: removeVideo
      description: 
        Allows you to update a video. Only video's owner and admininistrator will recieve a OK for this endpoint.
      parameters:
        - in: header
          name: token
          description: JWT Auth token.
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/VideoAdd'
        description: Inventory item to add
      responses:
        '200':
          description: Updated
        '403':
          description: Unauthorized
        '422':
          description: 'Entity not found'
    delete:
      tags:
        - video
      summary: Removes a video. 
      operationId: updateVideo
      description: 
        Allows you to remove a video. Only video's owner and admininistrator will recieve a OK for this endpoint.
      parameters:
        - in: header
          name: token
          description: JWT Auth token.
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Removed
        '403':
          description: Unauthorized
        '422':
          description: 'Entity not found'
  /video/tag:
    get:
      tags:
        - video
      summary: Gets video by given tag
      operationId: getVideoByTag
      description:
        Get video with given tag. Ditto as the search endpoint.
      parameters:
        - in: query
          name: sort
          description: Sorting name
          required: true
          schema:
            $ref: '#/components/schemas/VideoSorting'
        - in: query
          name: direction
          description: Sorting name
          required: true
          schema:
            $ref: '#/components/schemas/SortingDirections'
      responses:
        '200':
          description: Items found.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/PaginatedVideos'
        '400':
          description: 'Error'
  /video/me:
    get:
      tags:
        - video
      summary: Gets user videos
      operationId: getMyVideos
      description:
        Gets videos uploaded by user. Public and not public.
      parameters:
        - in: header
          name: token
          description: JWT Auth token.
          required: true
          schema:
            type: string
        - in: query
          name: sort
          description: Sorting name
          required: true
          schema:
            $ref: '#/components/schemas/VideoSorting'
        - in: query
          name: direction
          description: Sorting name
          required: true
          schema:
            $ref: '#/components/schemas/SortingDirections'
      responses:
        '200':
          description: Items found.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/PaginatedVideos'
        '400':
          description: 'Error'
  /auth/login:
    post:
      tags:
        - auth
      summary: Login user
      operationId: login
      description:
        Login user. Admininistrator and user uses the same endpoint.
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Login'
        description: Login payload
      responses:
        '200':
          description: User authenticated. Returns JWT token
          content:
            text/plain:
              schema:
                type: string
        '422':
          description: User not found
  /auth/register:
    post:
      tags:
        - auth
      summary: Register user
      operationId: register
      description:
        Register user.
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Register'
        description: Register payload
      responses:
        '200':
          description: User created.
        '400':
          description: Error
  /auth/forgot-password:
    post:
      tags:
        - auth
      summary: Forgot password.
      operationId: forgotPassword
      description:
        Forgot password.  Sends email with a password reset instructions.
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ForgotPassword'
        description: Forgot password payload
      responses:
        '200':
          description: Request for reset send.
        '400':
          description: Error
  /user/me:
    get:
      tags:
        - user
      summary: Get current user profile
      operationId: userMe
      description:
        Gets current logged user profile.
      parameters:
        - in: header
          name: token
          description: JWT Auth token.
          required: true
          schema:
            type: string
      responses:
        '200':
          description: User profile
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        '400':
          description: Error
  /user/update-password:
    post:
      tags:
        - user
      summary: Update users password
      operationId: userUpdatePassword
      description:
        Allows to update users password
      parameters:
        - in: header
          name: token
          description: JWT Auth token.
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UpdatePassword'
        description: Update password payload
      responses:
        '200':
          description: Password changed
        '400':
          description: Error
  /user/delete:
    delete:
      tags:
        - user
      summary: Delete users profile
      operationId: deleteUserMe
      description:
        Allows user to delete his owns account.
      parameters:
        - in: header
          name: token
          description: JWT Auth token.
          required: true
          schema:
            type: string
      responses:
        '200':
          description: User removed
        '400':
          description: Error
  /admin/ban/{uuid}:
    put:
      tags:
        - admin
      summary: Ban user by id
      operationId: banUser
      description:
        Banned user cannot login to his account
      parameters:
        - in: header
          name: token
          description: JWT Auth token.
          required: true
          schema:
            type: string
        - in: path
          name: uuid
          description: ID of an user.
          required: true
          schema:
            type: uuid
      responses:
        '200':
          description: User banned
        '400':
          description: Error
  /admin/unban/{uuid}:
    put:
      tags:
        - admin
      summary: Unban user by id
      operationId: unbanUser
      description:
        Unbanned user can access his account again
      parameters:
        - in: header
          name: token
          description: JWT Auth token.
          required: true
          schema:
            type: string
        - in: path
          name: uuid
          description: ID of an user.
          required: true
          schema:
            type: uuid
      responses:
        '200':
          description: User unbanned
        '400':
          description: Error
  /admin/block-video/{uuid}:
    put:
      tags:
        - admin
      summary: Block video by id
      operationId: blockVideo
      description:
        Blocked video cannot be watched by anybody but owner and administrators.
      parameters:
        - in: header
          name: token
          description: JWT Auth token.
          required: true
          schema:
            type: string
        - in: path
          name: uuid
          description: ID of a video.
          required: true
          schema:
            type: uuid
      responses:
        '200':
          description: Video blocked
        '400':
          description: Error
  /admin/unblock-video/{uuid}:
    put:
      tags:
        - admin
      summary: Block video by id
      operationId: unblockVideo
      description:
        Video can again be watched by anyone.
      parameters:
        - in: header
          name: token
          description: JWT Auth token.
          required: true
          schema:
            type: string
        - in: path
          name: uuid
          description: ID of a video.
          required: true
          schema:
            type: uuid
      responses:
        '200':
          description: Video unblocked
        '400':
          description: Error
  
components:
  schemas:
    VideoAdd:
      required:
        - name
        - description
        - tags
        - public
        - video
      properties:
        name:
          type: string
          example: Me at ZOO
        description:
          type: string
          example: Video of me at ZOO
        tags:
          type: array
          minItems: 1
          maxItems: 5
          uniqueItems: true
          items:
            type: string
          example: ["animals", "ZOO"]
        public:
          type: boolean
        video:
          type: binary
    Video:
      required:
        - id
        - name
        - description
        - tags
        - thumbnail
        - videoLink
        - uploadDate
        - views
      properties:
        id: 
          type: uuid
          example: 7a291aa7-d428-4e76-82ea-1f8d82994c79
        name:
          type: string
          example: Me at ZOO
        description:
          type: string
          example: Video of me at ZOO
        tags:
          type: array
          minItems: 1
          maxItems: 5
          uniqueItems: true
          items:
            type: string
          example: ["animals", "ZOO"]
        thumbnail:
          description: CDN link
          type: string
        videoLink:
          description: CDN link
          type: string
        uploadDate:
          description: Date of upload in server
          type: date
        views:
          description: No of views
          type: number
    PaginatedVideos:
      required:
        - total
        - perPage
        - pageNo
        - values
      properties:
        total:
          type: number
          min: 0
        perPage:
          type: number
          min: 1
          max: 50
        pageNo:
          type: number
        values: 
          type: array
          items:
            $ref: '#/components/schemas/Video'
    SortingDirections:
      type: string
      enum:
        - ASC
        - DESC
    VideoSorting:
      type: any
      enum:
        - name
        - uploadDate
    Login:
      required:
        - login
        - password
      properties:
        login:
          example: mylogin69
          type: string
        password:
          example: admin123
          type: string
    Register:
      required:
        - login
        - email
        - password
      properties:
        login:
          example: 'admin'
          type: string
        email:
          example: 'example@email.com'
          pattern: ^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$
          type: email
        password:
          example: 'admin123'
          type: string
          minLength: 8
    ForgotPassword:
      properties:
        required:
          - email
        email:
          example: 'example@email.com'
          pattern: ^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$
          type: email
    User:
      required:
        - id
        - email
        - login
      properties:
        id:
          example: 6299bf70-b211-4dfc-84cf-438e5d366559
          type: uuid
        email:
          example: example@email.com
          type: email
        login:
          example: exampleLogin
          type: string
        imageUrl:
          example: https://example.com/img/example420.jpg
          description: link to CDN
          type: string
    UpdatePassword:
      required:
        - oldPassword
        - newPassword
      properties:
        oldPassword:
          example: oldPassword
          type: string
        newPassword:
          example: newPassword1
          type: string
          minLength: 8
