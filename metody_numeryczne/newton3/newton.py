import lib


def isNodeInPoints(x):
    global points
    inside = False
    for i in range(len(points)):
        if points[i][0] == x:
            inside = True
    return inside


nodes = int(input("Podaj liczbe wezlow: "))
points = []
for i in range(nodes):
    while True:
        x = float(input(f"Podaj x{i}: "))
        y = float(input(f"Podaj f[x{i}]: "))
        if not isNodeInPoints(x):
            break
        print("Wezel juz istnieje")
    points.append((x, y))

result = lib.interpolation_polynomial(points, True)
print(result[1])
