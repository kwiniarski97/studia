import { expect } from "chai";
import { mount } from "@vue/test-utils";
import App from "@/App.vue";

describe("HelloWorld.vue", () => {
  it("renders props.msg when passed", () => {
    const wrapper = mount(App);
    wrapper.setData({
      wspolczynniki: [
        { level: 3, val: 1 },
        { level: 2, val: -4 },
        { level: 1, val: 3 },
        { level: 0, val: -5 }
      ],
      dzielnik: 2
    });
    wrapper.vm.licz();
    expect(wrapper.vm.wynik).eql([1, -2, -1, -7]);
  });
});
