from scipy.integrate import quad
import math


#2*x+3*x**2
print("Podaj funkcje np: x*450, funkcje matematyczne poprzedź przedrostkiem math np: sinus -> math.sin")
userInput = f"lambda x: {input()}"
fun = eval(userInput)

print("Podaj poczatkowa granice calkowania")
start_bound = float(input())
print("Podaj koncowa granice calkowania")
end_bound = float(input())

result = quad(fun, start_bound, end_bound)

print(f"Wynik: {result[0]}±{result[1]}")
