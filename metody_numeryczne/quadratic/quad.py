import numpy as np
import scipy.integrate
import pyparsing as pp

lowerBound = 0
upperBound = 0
func: lambda x: x


# TODO: CHANGE TO INPUT()
def bind_input():
    global lowerBound, upperBound, func
    lowerBound = 0
    upperBound = 1

    def func(x):
        return x


if __name__ == '__main__':
    bind_input()
    results = scipy.integrate.quadrature(func, lowerBound, upperBound)
    print(results)
