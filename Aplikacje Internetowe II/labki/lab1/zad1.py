# Napisz pętlę, która wypisze wszystkie dwucyfrowe liczby podzielne przez 7. Kolejne liczby powinny być wypisane w jednym wierszu i rozdzielone pojedynczymi spacjami.

podzielne = []
for i in range(10, 100):
    if i % 7 == 0:
        podzielne.append(str(i))
print(', '.join(podzielne))