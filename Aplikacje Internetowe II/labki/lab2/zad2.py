"""
Napisz funkcję sprytne_potegowanie(podstawa, wykladnik), która oblicza zadaną
potęgę w podany sposób: wywołuje rekurencyjnie samą siebie dla wykładnika
podzielonego na dwa (z zaokrągleniem) jak w przykładzie, a następnie operując na tym
częściowym wyniku oblicza pełną potęgę.
Przykład: 2^7 = 2^3 * 2^3 * 2. Jak widzimy, wystarczy raz obliczyć 2^3
"""


def sprytne_potegowanie(podstawa, wykladnik):
    print(podstawa, wykladnik)
    if(wykladnik == 1):
        return podstawa
    pods = sprytne_potegowanie(podstawa, wykladnik // 2)
    if(wykladnik % 2 == 0):
        return pow(pods, 2);
    return pow(pods, 2) * podstawa;


print(sprytne_potegowanie(2, 7))
