def __countOccurancesToDict(word):
    d = {}
    for letter in word:
        value = d.get(letter, 0)
        value += 1
        d[letter] = value
    return d


def czyAnagram(pierwszy, drugi):
    pd = __countOccurancesToDict(pierwszy)
    dd = __countOccurancesToDict(drugi)
    for l in dd:
        if l not in pd or pd[l] < dd[l]:
            return False
    return True


print(czyAnagram("elo", "ea"))
