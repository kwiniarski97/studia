import sys
import io


def substr_conc(str):
    result = ''
    for i in range(0, len(str) + 1):
        result += str[0: i]
    return result


def count_2sub_match(a, b):
    counter = 0
    already_checked_fragments = []
    for i in range(0, len(b)):
        fragment = b[i: i + 2]
        if fragment in already_checked_fragments or len(fragment) < 2:
            continue
        already_checked_fragments.append(fragment)
        for j in range(0, len(a)):
            if fragment == a[j: j + 2]:
                counter += 1
                break
    return counter


def is_build_pos(small, large, goal):
    while 0 < large <= goal:
        goal -= 5
        large -= 1
    while 0 < small <= goal:
        goal -= 1
        small -= 1
    return goal == 0


def text_file_avg_word_length():
    filename = sys.argv[1]
    file = io.open(filename, mode="r", encoding='utf-8')
    total_words = 0
    total_length = 0
    for line in file:
        words = line.split(' ')
        total_words += len(words)
        for word in words:
            total_length += len(word)
    file.close()
    print(0 if total_words == 0 else total_length / total_words)


def numerate_rows_in_file():
    filename = sys.argv[1]
    file = io.open(filename, mode="r", encoding='utf-8')
    new_file = io.open('enum ' + filename, mode="w", encoding='utf-8')
    i = 1
    for line in file:
        new_file.write(f"{i} {line}")
        i += 1
    new_file.close()
    file.close()
