from datetime import datetime
import sys


def arithmetics(numer1, number2, operation):
    if operation not in ["+", "-", "*", "/"]:
        return "Invalid operator"
    if operation == '/' and number2 == 0:
        return "Cannot divide by 0"
    if not isinstance(numer1, int) or not isinstance(number2, int):
        return "Please provide a valid numbers"
    equation = f"{numer1} {operation} {number2}"
    return eval(equation)


def days(since):
    since_arr = since.split("-")
    if len(since_arr) != 3:
        return "Invalid date format. Acceptable one is YYYY-MM-DD"
    formatted = datetime(int(since_arr[0]), int(since_arr[1]), int(since_arr[2]))
    now = datetime.now()
    delta = now - formatted
    return delta.days


def arguments():
    args = getArgs()
    arr = []
    for arg in args:
        if len(arg) >= 3:
            arr.append(arg)
    return " ".join(list(reversed(arr)))


def histogram(text):
    if type(text) is 'str':
        text = text.replace(" ", "")
    arr = dict()
    for c in text:
        if c in arr:
            arr[c] = arr[c] + 1
        else:
            arr[c] = 1
    return arr


def quadratic():
    a, b, c = map(lambda x: int(x), getArgs())
    delta = pow(b, 2) - 4 * a * c
    if delta < 0:
        return "delta < 0"
    elif delta == 0:
        x0 = (-1 * b) / (2 * a)
        return f"x0 = {x0}"
    else:
        delta_sqrt = pow(delta, 1 / 2)
        x1 = (-1 * b - delta_sqrt) / (2 * a)
        x2 = (-1 * b + delta_sqrt) / (2 * a)
        return f"x1 = {x1}. x2 = {x2}"


def getArgs():
    return sys.argv[1: len(sys.argv)]

