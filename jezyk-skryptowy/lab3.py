from itertools import islice

from lab1 import histogram
from values import card_ranks


def hand_rank(hand):
    hand_rank_list = [card[0] for card in hand]
    hand_color_list = [card[1] for card in hand]

    hand_rank_histogram = histogram(hand_rank_list)
    hand_color_histogram = histogram(hand_color_list)

    is_hand_rank_sequence = is_rank_sequence(hand)

    # --- sprawdzamy poker krolewski: 5 kart w tym samym kolorze, po kolei, najwyzsza to as
    if (5 in hand_color_histogram.values()) and ('A' in hand_rank_list) and is_hand_rank_sequence:
        return 10
    # --- sprawdzamy poker: 5 kart w tym samym kolorze, po kolei
    if (5 in hand_color_histogram.values()) and is_hand_rank_sequence:
        return 9
    # --- sprawdzamy karete: 4 karty tej samej rangi
    if 4 in hand_rank_histogram.values():
        return 8
    # --- sprawdzamy full house: 3 karty tej samej rangi i 2 karty tej samej rangi
    if 3 in hand_rank_histogram.values() and 2 in hand_rank_histogram.values():
        return 7
    # --- sprawdzamy kolor
    if 5 in hand_color_histogram.values():
        return 6
    # --- sprawdzamy strit
    if is_hand_rank_sequence:
        return 5
    # --- sprawdzamy trojke
    if 3 in hand_rank_histogram.values():
        return 4
    # --- sprawdzamy dwie pary
    if is_object_occurring_times(hand_rank_histogram.values(), 2, 2):
        return 3
    # --- sprawdzamy pare
    if 2 in hand_rank_histogram.values():
        return 2
    # --- high card
    return 1


def is_rank_sequence(hand):
    indexes = []
    for card in hand:
        indexes.append(card_ranks.index(card[0]))
    indexes.sort()
    for i in range(1, len(indexes)):
        if abs(indexes[i - 1] - indexes[i]) > 1:
            return False
    return True


def is_object_occurring_times(list, element, times):
    gen = (True for i in list if i == element)
    return next(islice(gen, times - 1, None), False)

