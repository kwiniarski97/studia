from random import shuffle
from values import card_ranks, card_suits


def histogram(sentence):
    letters = sentence.replace(' ', '')
    histo = {}
    for letter in letters:
        if letter in histo:
            histo[letter] = histo[letter] + 1
        else:
            histo[letter] = 1
    return histo


def deck():
    d = []
    for rank in card_ranks:
        for suit in card_suits:
            d.append([rank, suit])
    return d


def shuffle_deck(deck):
    shuffle(deck)
    return deck


def deal(deck, n):
    """Returns deck split into players. DOES NOT SHUFFLE CARD YOU SHOULD PASS ALREADY SHUFFLED"""
    if n > 10:
        raise ValueError('W pokera nie moze grac wiecej niz 10 graczy.')
    dealed = []
    for player in range(0, n):
        player_cards = []
        for _ in range(0, 5):
            player_cards.append(deck.pop())
        dealed.append(player_cards)
    return dealed
