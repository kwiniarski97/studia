import lab1
import lab2
import lab3
import values


class Card:
    value = []

    unicode_dict = {'s': '\u2660', 'h': '\u2665', 'd': '\u2666', 'c': '\u2663'}

    def __init__(self, rank, suit):
        self.rank = rank
        self.suit = suit
        pass

    def get_value(self):
        return self.rank, self.suit

    def __str__(self):
        return "( " + str(self.rank) + " " + self.unicode_dict[self.suit] + " )"


class Deck:
    deck = []

    def __init__(self, *args):
        self.deck = Deck.create_deck()
        pass

    def __str__(self):
        return Deck.cards_to_string(self.deck)

    def shuffle(self):
        lab2.shuffle(self.deck)
        pass

    # nonsense but ok, this method dont belong here. breaks SRP
    def deal(self, players):
        hands = lab2.deal(self.deck, len(players))
        for i in range(0, len(players)):
            players[i].set_hand(hands[i])
        return
        pass

    @staticmethod
    def create_deck():
        deck = []
        for value in values.card_ranks:
            for suit in values.card_suits:
                deck.append(Card(value, suit))
        return deck

    @staticmethod
    def cards_to_string(cards):
        return "[" + ", ".join(str(card) for card in cards) + "]"


class Player:

    def __init__(self, money, name=""):
        self.__stack_ = money
        self.__name_ = name
        self.__hand_ = []

    def take_card(self, card):
        self.__hand_.append(card)

    def get_stack_amount(self):
        return self.__stack_

    def get_player_hand_immutable(self):
        return tuple(self.__hand_)

    def cards_to_str(self):
        output = ""
        for card in self.__hand_:
            output = output + " " + str(card)
        return output

    def set_hand(self, hand):
        self.__hand_ = hand


def histogram(text):
    return lab1.histogram(text)
    pass


def get_player_hand_rank(hand):
    card_mapped_to_tuples = [(card.rank, card.suit) for card in hand]
    return lab3.hand_rank(card_mapped_to_tuples)
