module Lists where

import Data.Char

-- 4.1.1

{-
*Math> :t map
map :: (a -> b) -> [a] -> [b]
*Math> map (^2) [1,3,5]
[1,9,25]
*Math> map (\x -> x*x) [1,3,5]
[1,9,25]
*Math> let f = (^2) in map f [1,3,5]
[1,9,25]
*Math> sum [1,3,5]
9
*Math> sqrt (sum [1,3,5])
3.0
-}

sumOfSquares x = sum(map(^2) x)

{-
*List> sumOfSquares' [1,3,5]
35
-}

-- 4.1.2

{-
*List> :t (:)
(:) :: a -> [a] -> [a]
*List> 3:[]
[3]
*List> 3:5:[]
[3,5]
*List> 3:[5]
[3,5]
*List> let f (x:xs) = (x, xs)
*List> f [3,5,7]
(3,[5,7])
-}

-- http://www.balois.pl/jipp/haskell/listy.htm

sumOfSquares' (x:xs) = x * x + sumOfSquares' xs
sumOfSquares' _ = 0

{-
*List> sumOfSquares [1,3,5]
35
-}

-- 4.2.1

-- Slide 33/70

{-
    *     *
 0  1  2  3
[3, 5, 7, 9]
-}

{-
*List> let f0 (x:xs) = x
*List> let f1 (_:x:xs) = x
*List> let f2 (_:_:x:xs) = x
*List> f1 [3,5,7]
5
*List> f2 [3,5,7]
7
*List> f0 []
*** Exception: <interactive>:11:5-17: Non-exhaustive patterns in function f0
-}

-- http://www.balois.pl/jipp/haskell/listy.htm

-- solution here

sum1 :: Num a => [a] -> a
sum1 [] = 0
sum1 [_] = 0
sum1 (_:x:xs) = x + sum1 xs

{-
*List> sum1 []
0
*List> sum1 [3]
0
*List> sum1 [3,5,7,9]
14
-}

-- 4.2.2 *

sum2 :: Num a => [a] -> a
sum2 [] = 0
sum2 [x] = x
sum2 (x: _:xs) = x + sum2 xs

-- 4.2.3 *

sum3 :: Num a => [a] -> a
sum3 [] = 0
sum3 [x] = 0
sum3 (x:_:_:xs) = x + sum3 xs

-- 4.3.1

{-
*List> :t "cat"
"cat" :: [Char]
*List> ['c','a','t']
"cat"
*List> 3 : [5,7]
[3,5,7]
*List> [3,5] ++ [7]
[3,5,7]
*List> 'c' : "at"
"cat"
*List> "c" ++ "at"
"cat"
*List> let small x = 'a' <= x && x <= 'z'
*List> small 'c'
True
*List> small 'X'
False
-}

-- solution here

countLower x = length $ filter isLower x

{-
*List> countLower "cat Bob"
5
-}

-- 4.3.2 *

countLowerUpper :: String -> (Int,Int)
countLowerUpper str = (lowerCount,upperCount)
    where lowerCount = length (filter (isLower) str)
          upperCount = length (filter (isUpper) str) 

-- 4.4 *

string2bools :: [Char] -> [Bool]
string2bools str = map isLower str

-- 4.5.1

cgtx y [] = 0
cgtx y (x:xs) | x < y = 1 + cgtx y xs

{-
cgtx 3 [1,2,3,4,5]
2
-}

-- 4.5.2 *

cltx x [] = 0
cltx x (a:as) | a < x = 1 + cltx x as
              | otherwise = cltx x as

-- 4.5.3

gtx a []  = [] 
gtx a (x:xs) |  (x > a) = x : gtx a xs
             | otherwise = gtx a xs


{-
*List> gtx 3 [1,2,3,4,5]
[4,5]
-}

-- 4.5.4 *

ltx x [] = []
ltx x (a:as) | x > a = a : ltx x as
             | otherwise = ltx x as

-- 4.6

{-
*List> :t read
read :: Read a => String -> a
*List> :t show
show :: Show a => a -> String
*List> read "35"
*** Exception: Prelude.read: no parse
*List> read "35" :: Int
35
*List> show 35
"35"
-}

string2int x = read x :: Integer

{-
*List> string2int "35"
35
-}

-- solution here, second definition

-- string2int' :: String -> Int
-- complete the solution

{-
*List> string2int' "35"
35
-}

-- 4.7.1

{-
suma1 = 1 + 1/2 + 1/3 + ... + 1/100
-}

suma1 = sum (map (1/) [1..100])

suma1' = sum(map (\x -> 1/x) [1..100])

suma1'' = sum([1/x | x <- [1..100]])

-- suma1 = ...    -- with the use of section, slide 11/70
-- suma1' = ...   -- with the use of lambda notation, slide 42/70
-- suma1'' = ...  -- with the use of set notation, slide 44/70

{-
*List> suma1
5.187377517639621
*List> suma1'
5.187377517639621
*List> suma1''
5.187377517639621
-}

-- 4.7.2

iloczyn1 = product (map(\x -> (1+x)/(2+x)) [1..50])

iloczyn1' = product([(1+x)/(2+x) | x <- [1..50]])

-- iloczyn1 = ...    -- with the use of lambda notation, slide 42/70
-- iloczyn1' = ...   -- with the use of set notation, slide 44/70
-- iloczyn1'' = ...  -- with the use of one of the fold functions instead of product, slide 40,41/70
                     -- http://www.balois.pl/jipp/haskell/fold.htm


{-
*Lists> iloczyn1
3.846153846153846e-2
*Lists> iloczyn1'
3.846153846153846e-2
*Lists> iloczyn1''
3.846153846153844e-2
-}

-- 4.7.3 *

sum([ 1/(x^2) | x <- [1..1000] ])

sum(map( \x -> 1/(x^2))  [1..1000])

-- 4.7.4 *

sum([ sqrt x - 1/x | x <- [1..1000] ])

sum(map(\x -> (sqrt x) - (1/x)) [1..1000])

-- 4.7.5 *

sum([ (x + 1) / (x ^ 3) | x <- [1..1000] ])

sum(map(\x -> (x + 1) / (x ^ 3)) [1..1000])

-- 4.8

-- slide 44/70

-- solution here

-- 4.8.1 *

-- homework

-- 4.8.2 *

-- homework
