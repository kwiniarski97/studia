module Lists where

import Data.Char

sumOfSquares x = sum(map(^2) x)

sumOfSquares' (x:xs) = x * x + sumOfSquares' xs
sumOfSquares' _ = 0

sum1 :: Num a => [a] -> a
sum1 [] = 0
sum1 [_] = 0
sum1 (_:x:xs) = x + sum1 xs

sum2 :: Num a => [a] -> a
sum2 [] = 0
sum2 [x] = x
sum2 (x: _:xs) = x + sum2 xs

sum3 :: Num a => [a] -> a
sum3 [] = 0
sum3 [x] = 0
sum3 (x:_:_:xs) = x + sum3 xs

countLower x = length $ filter isLower x

cgtx y [] = 0
cgtx y (x:xs) | x < y = 1 + cgtx y xs
              | otherwise = cgtx y xs

gtx a []  = [] 
gtx a (x:xs) |  (x > a) = x : gtx a xs
             | otherwise = gtx a xs

-- lab 5
rectangleRule :: (Float -> Float) -> Float -> Float -> Int -> Float
rectangleRule (f) a b n = h * sum (map f points)
    where
      h = (b - a) / fromIntegral(n)
      points = [a + fromIntegral(i) * h | i <- [1 .. n]]

integral = rectangleRule sin 0 pi 100
integral' = rectangleRule sin 0 pi 1000
integral2 = rectangleRule sqrt 0 1 100
integral2' = rectangleRule sqrt 0 1 10000
integral3 = rectangleRule (^2) 0 1 100
integral3' = rectangleRule (^2) 0 1 1000
integral4 = rectangleRule (\x -> x ^ 3 + 2 * x) 0 2 100
integral4' = rectangleRule (\x -> x ^ 3 + 2 * x) 0 2 1000

main = do
    putStrLn (show (integral4))
    putStrLn (show (integral4'))



module Trees where

data Tree a = Leaf a
            | Node a (Tree a) (Tree a)
            | Null

treeSize :: Tree a -> Int
treeSize Null = 0
treeSize (Leaf _) = 1
treeSize (Node _ left right) = 1 + treeSize left + treeSize right

showTree :: Show a => Tree a -> String
showTree Null = "()"
showTree (Leaf x) = show x
showTree (Node x left right) = (show x) ++" L(" ++ showTree left ++") R(" ++ showTree right ++ ")"

add :: Ord a => a -> Tree a -> Tree a
add leaf (Node x left right) = | leaf == Null = tree
                     | leaf
bt1 = Node 7 (Node 4 (Leaf 2) (Leaf 5)) (Leaf 10)
 
bt2 = Node 7 (Node 4 (Leaf 2) (Leaf 5)) 
             (Node 10 (Leaf 9) (Node 13 (Leaf 11) (Leaf 15)))

bt3 = Node 7 (Leaf 1) Null

main = do
    putStrLn (showTree (bt1))
    putStrLn (showTree (bt2))
    putStrLn (showTree (bt3))