panstwo('Polska').
panstwo('Niemcy').
panstwo('Francja').
panstwo('Wlochy').
panstwo('Hiszpania').
panstwo('Wielka Brytania').

miasto('Warszawa').
miasto('Kraków').
miasto('Berlin').
miasto('Paryż').
miasto('Rzym').
miasto('Wenecja').
miasto('Barcelona').
miasto('Madryt').
miasto('Londyn').

zabytek('Pałac w Wilanowie').
zabytek('Kolumna Zygmunta III Wazy').
zabytek('Wawel').
zabytek('Sukiennice').
zabytek('Brama Brandenburska').
zabytek('Reichstag').
zabytek('Wieża Eiffla').
zabytek('Katedra Notre-Dame').
zabytek('Pałac Elizejski').
zabytek('Bazylika św. Pawła za Murami').
zabytek('Koloseum').
zabytek('Zamek Świętego Anioła').
zabytek('Bazylika św. Marka').
zabytek('Pałac Dożów').
zabytek('Sagrada Familia').
zabytek('Pałac Kryształowy').
zabytek('Tower Bridge').
zabytek('Pałac Buckingham').
zabytek('Katedra Świętego Pawła').

polozenie('Warszawa','Polska').
polozenie('Kraków','Polska').
polozenie('Berlin','Niemcy').
polozenie('Paryż','Francja').
polozenie('Rzym','Włochy').
polozenie('Wenecja','Włochy').
polozenie('Barcelona','Hiszpania').
polozenie('Madryt','Hiszpania').
polozenie('Londyn','Wielka Brytania').

obok('Polska', 'Niemcy').
obok('Niemcy', 'Francja').
obok('Francja', 'Wielka Brytania').
obok('Francja', 'Włochy').
obok('Francja', 'Hiszpania').

gdzie('Warszawa','Pałac w Wilanowie').
gdzie('Warszawa','Wawel').
gdzie('Warszawa','Kolumna Zygmunta III Wazy').
gdzie('Kraków','Sukiennice').
gdzie('Berlin','Brama Brandenburska').
gdzie('Berlin','Reichstag').
gdzie('Paryż','Wieża Eiffla').
gdzie('Paryż','Katedra Notre-Dame').
gdzie('Paryż','Pałac Elizejski').
gdzie('Rzym','Bazylika św. Pawła za Murami').
gdzie('Wenecja','Zamek Świętego Anioła').
gdzie('Wenecja','Bazylika św. Marka').
gdzie('Wenecja','Pałac Dożów').
gdzie('Barcelona','Sagrada Familia').
gdzie('Londyn','Tower Bridge').
gdzie('Londyn','Pałac Buckingham').
gdzie('Londyn','Katedra Świętego Pawła').