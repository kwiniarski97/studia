public abstract class Person {
    public String name;
    public int age;

    protected void breathe() {
        System.out.println("breathe");
    }

    protected void eat() {
        System.out.println("eat");
    }

    protected void sleep() {
        System.out.println("sleep");
    }
}
