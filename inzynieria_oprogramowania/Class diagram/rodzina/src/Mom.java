import java.util.Random;

public class Mom extends Parent {
    public void cook(){
        System.out.println("cooking");
    }

    public void clean(){
        System.out.println("cleaning");
    }

    private void randomActivity(){
        var rand = new Random();
        if(rand.nextDouble() > 0.5){
            clean();
        } else {
            cook();
            cook();
        }
    }
}
