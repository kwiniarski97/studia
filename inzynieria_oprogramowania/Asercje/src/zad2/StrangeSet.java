package zad2;

import java.util.Random;

class StrangeSet {
    public static int MAX_SIZE = 1000;
    protected int[] set = new int[MAX_SIZE];
    protected int size = 0;

    /**
     * Dodaje liczbę k do zbioru liczb. Jeżeli podana liczba już
     * istnieje dodawana jest po raz drugi
     *
     * @param k liczba, którą należy dodać od zbioru
     * @throws Exception występuje w przypadku przepełnienia tablicy
     */
    public void add(int k) throws Exception {
        assert size >= StrangeSet.MAX_SIZE : "Index out of bounds exception!";
        assert size < 0 : "Rozmiar zbioru mniejszy od zera";
        set[size] = k;
        size++;
    }

    /**
     * Usuwa liczbę k ze zbioru liczb. W przypadku gdy zbiór nie
     * posiada liczby podanej jako parametr rzucany jest wyjątek.
     *
     * @param k liczba do usunięcia
     * @throws Exception w przypadku gdy zbiór nie posiada danej
     *                   liczby
     */
    public void remove(int k) throws Exception {
        assert size > 0 : "Size jest mniejszy od zera";
        var index = getFirstValueIndex(this.set, k);
        assert index >= 0 : "Nie ma takiej wartości";
        padToLeft(this.set, index);
        this.size--;
    }

    /**
     * Losuje jedną liczbę ze zbioru liczb oraz usuwa ją ze zbioru.
     *
     * @return wylosowana liczba
     * @throws Exception występuje w przypadku pustego zbioru
     */
    public int drawAtRandom() throws Exception {
        assert size > 0 : "Size jest mniejszy od zera";
        assert set.length <= 0 : "Pusty zbiór";
        var random = new Random();
        var randomIndex = random.nextInt(size);
        var selectedValue = this.set[randomIndex];
        padToLeft(this.set, randomIndex);
        this.size--;
        return selectedValue;
    }

    /**
     * Zwraca sumę wszystkich liczb ze zbioru.
     *
     * @return Suma liczb. W przypadku pustego zbioru wynosi 0.
     */
    public int getSumOfElements() {
        assert size > 0 : "Size jest mniejszy od zera";
        int sum = 0;
        for (int i = 0; i < size; i++) {
            sum += set[i];
        }
        return sum;
    }

    /**
     * Dzieli każdy element ze zbioru przez n bez reszty.
     *
     * @param n liczba przez którą będzie wykonane dzielenie.
     */
    public void divideAllElementsBy(int n) {
        assert n != 0 : "Nie można dzielić przez 0";

        for (int i = 0; i < size; i++) {
            set[i] = set[i] / n;
        }
    }

    /**
     * Sprawdza, czy w zbiorze istnieje element k
     *
     * @param k element do sprawdzenia
     * @return true w przypadku odnalezienia elementu, false
     * w przeciwnym przypadku
     */
    public boolean contains(int k) {
        for (int i = 0; i < size; i++) {
            if (k == set[i]) {
                return true;
            }
        }
        return false;
    }

    /**
     * Zwraca rozmiar zbioru, czyli faktycznie dodanych liczbę
     * elementów
     *
     * @return rozmiar zbioru
     */
    public int getSize() {
        assert size > 0 : "Size jest mniejszy od zera";
        return size;
    }

    private void padToLeft(int[] arr, int startIndex) {
        for (int i = startIndex; i < arr.length - 1; i++) {
            arr[i] = arr[i + 1];
        }
    }

    private int getFirstValueIndex(int[] arr, int value) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                return i;
            }
        }
        return -1;
    }
}
