package zad2;

public class StrangeSetTest {
    public static void main(String[] args) throws Exception {
        var set = new StrangeSet();
        set.add(1);
        set.add(1);
        set.add(2);
        set.add(3);

        set.remove(2);

        System.out.println(set.contains(3));
        System.out.println(set.contains(2));


        System.out.println("random " + set.drawAtRandom());

        System.out.println("sum = " + set.getSumOfElements());

        set.divideAllElementsBy(2);

        for (var i = 0; i < set.size; i++) {
            System.out.println(set.set[i]);
        }
    }
}
