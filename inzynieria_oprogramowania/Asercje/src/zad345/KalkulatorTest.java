package zad345;

import org.junit.*;

import java.lang.reflect.Array;
import java.security.InvalidParameterException;
import java.util.Random;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class KalkulatorTest {

    public static int i;

    Kalkulator kalkulator;
    Random random;

    public KalkulatorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        System.out.println(">> TEST START >>");
        i = 1;
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println(">> TEST STOP >>");
    }

    @Before
    public void setUp() {
        kalkulator = new Kalkulator();
        random = new Random();
        System.out.println("\tTest metody nr: " + i);
    }

    @After
    public void tearDown() {
        System.out.println("\tKoniec testu metody nr: " + i);
        i++;
    }

    /**
     * Test of main method, of class Kalkulator.
     */
    @Test
    public void testMain() {
        System.out.println("\t\tMetoda: main");
    }

    /**
     * Test of add method, of class Kalkulator.
     */
    @Test
    public void testAdd() {
        System.out.println("\t\tMetoda: add");
        for (int j = -100; j < 100; j++) {
            for (int k = -100; k < 100; k++) {
                Kalkulator instance = new Kalkulator();
                int result = instance.add(j, k);
                int expResult = j + k;
                assertEquals("Blad przy dodawaniu", expResult,
                        result);
            }
        }
        System.out.println("\t\t\tWszystko OK");
    }

    /**
     * Test of sub method, of class Kalkulator.
     */
    @Test
    public void testSub() {
        System.out.println("\t\tMetoda: sub");
        for (int j = -100; j < 100; j++) {
            for (int k = -100; k < 100; k++) {
                Kalkulator instance = new Kalkulator();
                int result = instance.sub(j, k);
                int expResult = j - k;
                assertEquals("Blad przy odejmowaniu", expResult,
                        result);
            }
        }
        System.out.println("\t\t\tWszystko OK");
    }


    @Test
    public void multiTest() {
        var a = 2;
        var b = 2;
        var expected = 4;

        var result = kalkulator.multi(a, b);

        assertEquals("Błąd mnożenia", expected, result);
    }

    @Test
    public void multiMultipleTests() {
        for (int i = 0; i < 100; i++) {
            var a = random.nextInt();
            var b = random.nextInt();
            var expected = a * b;

            var result = kalkulator.multi(a, b);

            assertEquals("Błąd mnożenia", expected, result);
        }
    }

    @Test
    public void divTestSuccess() {
        var a = 10;
        var b = 5;
        var expected = 2;

        var result = kalkulator.div(a, b);

        assertEquals("Błąd dzielenia", expected, result, 0.1);
    }

    @Test(expected = ArithmeticException.class)
    public void divShouldThrowExceptionWhenDividingBy0() {
        var a = 10;
        var b = 0;

        kalkulator.div(a, b);
    }

    @Test
    public void powTest1() {
        var a = 2;
        var b = 2;
        var expected = 4;

        var result = kalkulator.pow(a, b);

        assertEquals("Błąd potęgowania", expected, result);
    }

    @Test
    public void powTest2() {
        var a = 100;
        var b = 3;
        var expected = 1000000;

        var result = kalkulator.pow(a, b);

        assertEquals("Błąd potęgowania", expected, result);
    }

    @Test
    public void minTest1() {
        var minArr = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
        var expected = 0;

        var result = kalkulator.min(minArr);

        assertEquals("Błąd znajdywania min", expected, result);
    }

    @Test(expected = InvalidParameterException.class)
    public void minTest2() {
        var minArr = new int[]{};
        kalkulator.min(minArr);
    }

    @Test
    public void maxTest1() {
        var minArr = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
        var expected = 9;

        var result = kalkulator.max(minArr);

        assertEquals("Błąd znajdywania min", expected, result);
    }

    @Test(expected = InvalidParameterException.class)
    public void maxTest2() {
        var minArr = new int[]{};
        kalkulator.max(minArr);
    }

    @Test
    public void addMatrixTest1() {
        var a = new int[][]{new int[]{1, 1}, new int[]{1, 1}};
        var b = new int[][]{new int[]{1, 1}, new int[]{1, 1}};
        var expected = new int[][]{new int[]{2, 2}, new int[]{2, 2}};

        var result = kalkulator.addMatrix(a, b);

        assertArrayEquals("Błąd znajdywania min", expected, result);
    }

    @Test
    public void addMatrixTest2() {
        var a = new int[][]{new int[]{1, 1}, new int[]{1, 1}};
        var b = new int[][]{};

        var result = kalkulator.addMatrix(a, b);

        assertArrayEquals("Błąd znajdywania min", a, result);
    }

    @Test
    public void prodMatrixTest1() {
        var a = new int[][]{new int[]{1, 1}, new int[]{1, 1}};
        var b = 2;
        var expected = new int[][]{new int[]{2, 2}, new int[]{2, 2}};

        var result = kalkulator.prodMatrix(a, b);

        assertArrayEquals("Błąd znajdywania min", expected, result);
    }


}
