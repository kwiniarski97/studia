package zad345;

import java.security.InvalidParameterException;

public class Kalkulator {
    public static void main(String[] args) {

    }

    public int add(int a, int b) {
        int result = a + b;
        return result;
    }

    public int sub(int a, int b) {
        int result = a - b;
        return result;
    }

    public int multi(int a, int b) {
        return a * b;
    }

    public float div(int a, int b) {
        if (b == 0) {
            throw new ArithmeticException("Dzielenie przez 0");
        }
        return (float) a / b;
    }

    public int pow(int a, int b) {
        return (int) Math.pow(a, b);
    }

    public int fact(int a) {
        int result = 1;
        for (; a > 0; a--) {
            result *= a;
        }
        return result;
    }

    public int min(int[] arr) {
        if (arr.length <= 0) {
            throw new InvalidParameterException("arr musi mieć conajmniej jeden element");
        }

        var min = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < min) {
                min = arr[i];
            }
        }
        return min;
    }

    public int max(int[] arr) {
        if (arr.length <= 0) {
            throw new InvalidParameterException("arr musi mieć conajmniej jeden element");
        }

        var max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        return max;
    }

    public int[][] addMatrix(int[][] A, int[][] B) {
        if ((A.length <= 0) || (A[0].length <= 0)) {
            return B;
        }
        if ((B.length <= 0) || (B[0].length <= 0)) {
            return A;
        }

        int[][] C = new int[A.length][A[0].length];

        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A[i].length; j++) {
                C[i][j] = A[i][j] + B[i][j];
            }
        }
        return C;
    }

    public int[][] prodMatrix(int[][] tab1, int n) {
        int[][] C = new int[tab1.length][tab1[0].length];
        for (int i = 0; i < tab1.length; i++) {
            for (int j = 0; j < tab1[i].length; j++) {
                C[i][j] = tab1[i][j] * n;
            }
        }
        return C;
    }
}
