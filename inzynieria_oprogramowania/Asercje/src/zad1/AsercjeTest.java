package zad1;

public class AsercjeTest {
    public static void main(String[] args) {
        System.out.println(dziel(2, 0));
    }

    public static int dziel(int a, int b) {
        int licznik = a;
        int dzielnik = b; // To jest zabronione, gdyz b=0
        int wynik;

        // asercja przed sytuacją niebezpieczną
        assert dzielnik != 0 : "Pamietaj! NIE DZIEL PRZEZ ZERO !!!";
        wynik = licznik / dzielnik;

        return wynik;
    }
}
