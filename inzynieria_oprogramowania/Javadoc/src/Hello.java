import java.util.InvalidPropertiesFormatException;

/**
 * Class used for printing Hello World to standard output
 * @version 2.137
 * @author Karol Winiarski
 */
public class Hello {
    /**
     * Hold the value to be printed by {@link #PrintHello()}
     * Default value "Hello World"
     */
    public static String HelloWorld = "Hello World";

    /**
     * Prints static property {@link #HelloWorld} to standard output
     */
    public static void PrintHello() {
        System.out.println(Hello.HelloWorld);
    }

    /**
     * Method used to build a proper greeting for the given recipient.
     * @param recipient recipe to whom say hello world to
     * @return the string of greeting and recipient
     * @throws InvalidPropertiesFormatException if recipient is empty then the exception will be thrown
     */
    public String sayHello(String recipient) throws InvalidPropertiesFormatException{
        if(recipient.length() == 0){
            throw new InvalidPropertiesFormatException("helloRecipient has to be set");
        }
        return "Hello " + recipient;
    }

}
