package zad1;

import java.util.ArrayList;

public class Prototyp {
    public static void main(String[] args) {
        ArrayList<Cookie> cookies = new ArrayList<>();
        Cookie tempCookie = null;
        Cookie prototyp = new CoconutCookie();
        CookieMachine cm = new CookieMachine(prototyp);

        for (int i = 0; i <= 100; i++) {
            tempCookie = cm.makeCookie();
            tempCookie.setName("zad1.CoconutCookie " + i);
            cookies.add(tempCookie);
        }

        for (Cookie cookie : cookies) {
            System.out.println(cookie);
        }
    }
}
