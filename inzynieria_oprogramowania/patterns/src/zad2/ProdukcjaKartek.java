package zad2;

public class ProdukcjaKartek {
    private Kartka kartka;

    public ProdukcjaKartek(Kartka kartka) {
        this.kartka = kartka;
    }

    public Kartka zrobKsero(){
        return (Kartka) this.kartka.kseruj();
    }
}
