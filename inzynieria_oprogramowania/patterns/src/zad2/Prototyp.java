package zad2;


import java.util.ArrayList;

public class Prototyp {
    public static void main(String[] args) {
        ArrayList<Kartka> kartki = new ArrayList<>();
        Kartka tempKartka = new Kartka();
        tempKartka.setName("Kartka bazowa");
        ProdukcjaKartek pk = new ProdukcjaKartek(tempKartka);

        for (int i = 0; i <= 100; i++) {
            tempKartka = pk.zrobKsero();
            tempKartka.setName("Kartka" + i);
            kartki.add(tempKartka);
        }

        for (Kartka kartka : kartki) {
            System.out.println(kartka);
        }
    }
}
