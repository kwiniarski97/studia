import random
import sys

counters = {'A': 0, 'B': 0, 'C': 0}
A1 = dict()
A2 = dict()
B1 = dict()
B2 = dict()
C1 = dict()
C2 = dict()


def readfile():
    with open('learning.txt') as f:
        lines = list(f)
    return lines


def parse(line):
    return line.replace('\n', '').replace('[', '').replace(')', '').split('|')


def learn(parsed_line):
    counters[parsed_line[2]] += 1
    attr1 = tuple(float(i) for i in parsed_line[0].split('; '))
    attr2 = tuple(float(i) for i in parsed_line[1].split('; '))
    if parsed_line[2] == 'A':
        if attr1 in A1:
            A1[attr1] += 1
        else:
            A1[attr1] = 1
        if attr2 in A2:
            A2[attr2] += 1
        else:
            A2[attr2] = 1
    if parsed_line[2] == 'B':
        if attr1 in B1:
            B1[attr1] += 1
        else:
            B1[attr1] = 1
        if attr2 in B2:
            B2[attr2] += 1
        else:
            B2[attr2] = 1
    if parsed_line[2] == 'C':
        if attr1 in C1:
            C1[attr1] += 1
        else:
            C1[attr1] = 1
        if attr2 in C2:
            C2[attr2] += 1
        else:
            C2[attr2] = 1


def get_predition(a, b):
    total_options = sum(counters.values())
    a1_probability = calculate_probability(a, A1)
    a2_probability = calculate_probability(b, A2)
    b1_probability = calculate_probability(a, B1)
    b2_probability = calculate_probability(b, B2)
    c1_probability = calculate_probability(a, C1)
    c2_probability = calculate_probability(b, C2)
    xa = a1_probability / counters['A'] * a2_probability / counters['A'] * counters['A'] / total_options
    xb = b1_probability / counters['B'] * b2_probability / counters['B'] * counters['B'] / total_options
    xc = c1_probability / counters['C'] * c2_probability / counters['C'] * counters['C'] / total_options
    if xa == xb and xa == xc:
        return random.choice(["A", "B", "C"])
    if xa > xb and xa > xc:
        return "A"
    elif xb > xc and xb > xa:
        return "B"
    else:
        return "C"


def calculate_probability(arg, collecion):
    for q in collecion.keys():
        if q[0] <= arg < q[1]:
            return collecion[q]
    return 0


def main(argc, argv):
    lines = readfile()
    for line in lines:
        learn(parse(line))
    print('Co chcesz robic?')
    quit = False
    while quit != True:
        print("1 - Klasyfikuj pare liczb. \nq - wyjdz \n")
        odp = input()
        if odp == '1':
            a = float(input("Podaj pierwszą liczbę : "))
            b = float(input("Podaj drugą liczbę : "))
            print("Zakwalifikowano do : ", get_predition(a, b))
        elif odp == 'q':
            quit = True


if __name__ == "__main__":
    main(len(sys.argv), sys.argv)
