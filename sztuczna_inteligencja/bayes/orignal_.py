import math
import sys
from random import randrange, random


def read_file():
    with open('learning.txt') as f:
        lines = list(f)
    return lines


def parse_file(rows):
    results = []
    for row in rows:
        row = row.replace('\n', '')
        row = row.replace('[', '')
        row = row.replace(')', '')
        columns = row.split('|')
        char_a = sum(float(x) for x in columns[0].split(';')) / 2
        char_b = sum(float(x) for x in columns[1].split(';')) / 2
        result = columns[2]
        results.append([char_a, char_b, result])
        print(char_a, char_b)
    return results


def split_data(data, split_ratio):
    train_size = int(len(data) * split_ratio)
    train_set = []
    working_data = list(data)
    while len(train_set) < train_size:
        # get random from data
        index = randrange(len(working_data))
        train_set.append(working_data.pop(index))
    return [train_set, working_data]


def split_by_class(data):
    separated = {}
    for i in range(len(data)):
        vector = data[i]
        if vector[-1] not in separated:
            separated[vector[-1]] = []
        separated[vector[-1]].append(vector[:-1])
    return separated


def calculate_mean(numbers):
    return sum(numbers) / float(len(numbers))


def calculate_stdev(numbers):
    avg = calculate_mean(numbers)
    variance = sum([pow(x - avg, 2) for x in numbers]) / float(len(numbers) - 1)
    return math.sqrt(variance)


def summarize(data):
    summaries = [(calculate_mean(attribute), calculate_stdev(attribute)) for attribute in zip(*data)]
    return summaries


def get_summary(data):
    separated = split_by_class(data)
    summaries = {}
    for classValue, instances in separated.items():
        summaries[classValue] = summarize(instances)
    return summaries


def get_user_pick():
    print("Podaj pierwsza liczbe: ")
    num1 = float(input())
    print("Podaj druga liczbe: ")
    num2 = float(input())
    return [num1, num2]


def calculate_probability(x, mean, stdev):
    return (1 / (math.sqrt(2 * math.pi) * stdev)) * math.exp(-(math.pow(x - mean, 2) / (2 * math.pow(stdev, 2))))


def calculate_class_probabilities(summaries, test):
    probabilities = {}
    for classValue, classSummaries in summaries.items():
        probabilities[classValue] = 1
        for i in range(len(classSummaries)):
            mean, stdev = classSummaries[i]
            if stdev != 0:
                x = test[i]
                probabilities[classValue] *= calculate_probability(x, mean, stdev)
    return probabilities


def predict(summary, test):
    probabilities = calculate_class_probabilities(summary, test)
    best_label, best_prob = None, -1
    for classValue, probability in probabilities.items():
        if best_label is None or probability > best_prob:
            best_prob = probability
            best_label = classValue
    return best_label


def main(argc, argv):
    file = read_file()
    train = parse_file(file)
    summary = get_summary(train)
    while True:
        print("""
        * Wpisz "elo" aby wyjść.
        * Wpisz "lecim" aby stestować liczbę.
        """)
        user_control = str(input())
        if user_control == "elo":
            break
        elif user_control == 'lecim':
            test = get_user_pick()
            preditction = predict(summary, test)
            print(f"Obiekt nalezy do grupy {preditction}")


if __name__ == "__main__":
    main(len(sys.argv), sys.argv)
