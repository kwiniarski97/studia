select z.kierunek_id, count(*) as ile, nazwa from ZALICZENIA z
left join kierunki K on k.KIERUNEK_ID = z.KIERUNEK_ID group  by z.KIERUNEK_ID, nazwa
 order by ile desc

from(select  z.KIERUNEK_ID, count(*) lz from zaliczenia z group by z.KIERUNEK_ID) x
left join kierunki k on x.KIERUNEK_ID  on k.kierunek_id where lz > 7

from (select z.kierunek_id, count(*) from ZALICZENIA z group by z.KIERUNEK_ID
having count(*) > 7) x
left join  kierunki k on x.kierunek_id = k.kierunek_id

-- ile negatywnych zaliczen z kazdego przedmiotu
select przedmiot_id, count(*) lno
from zaliczenia
where wynik like('0%')
group by PRZEDMIOT
having count(*) = (
select max(lno)
from(select przedmiot_id, count(*) lno from zaliczenia where wynik like('N%') group by PRZEDMIOT_ID)
)

select x.* from
(select przedmiot_id, count(*) lno
from zaliczenia
where wynik like('N%')
group by PRZEDMIOT_ID) x,
(select przedmiot_id, count(*) lno
from zaliczenia
where wynik like('N%')
group by PRZEDMIOT_ID) y)
where x.lno > y.lno

--zadanie 4
select NAZWISKO, IMIE, STUDENT_ID
from STUDENCI
minus
select NAZWISKO, IMIE, s.STUDENT_ID
from STUDENCI s
left join ZALICZENIA z on s.STUDENT_ID = z.STUDENT_ID
where z.PRZEDMIOT_ID = 7


select NAZWISKO, IMIE, STUDENT_ID
from STUDENCI s
where STUDENT_ID not in (
select distinct STUDENT_ID
from ZALICZENIA
where PRZEDMIOT_ID = 7 and s.STUDENT_ID = STUDENT_ID )

--zad 5
select IMIE, NAZWISKO from STUDENCI s
where s.STUDENT_ID
in (
select STUDENT_ID from ZALICZENIA
where DATA_ZAL between '2009/04/20' and '2009/05/20' and STUDENT_ID = s.STUDENT_ID
)

--zad 7
with x as (select to_char(DATA_ZAL, 'YYYYMM') okres, count(*) lz
from ZALICZENIA
group by to_char(DATA_ZAL, 'YYYYMM'))

select * from x where x.lz = (select max(lz) from x)

-- zad 16
select NAZWISKO, IMIE, w.WYKLADOWCA_ID, p.PRZEDMIOT_ID, NAZWA, count(*) lz
from WYKLADOWCY w, ZALICZENIA z, PRZEDMIOTY p
where w.WYKLADOWCA_ID = z.WYKLADOWCA_ID and p.PRZEDMIOT_ID = z.PRZEDMIOT_ID
group by NAZWISKO, IMIE, w.WYKLADOWCA_ID, p.PRZEDMIOT_ID, NAZWA
having count(*) = (
select max(count(*))
from ZALICZENIA
where PRZEDMIOT_ID = p.PRZEDMIOT_ID
group by WYKLADOWCA_ID
)

--zad 20
select s.STUDENT_ID, NAZWISKO, IMIE, p.PRZEDMIOT_ID, NAZWA, mdz
from STUDENCI s, PRZEDMIOTY p,
    (select STUDENT_ID, x.* from ZALICZENIA z,
        (select PRZEDMIOT_ID, MIN(DATA_ZAL) mdz
        from ZALICZENIA
        group by PRZEDMIOT_ID) x
    where x.PRZEDMIOT_ID = z.PRZEDMIOT_ID and x.mdz = z.DATA_ZAL) z
where s.STUDENT_ID = z.STUDENT_ID and p.PRZEDMIOT_ID = z.PRZEDMIOT_ID

-- zad30
select p.PRZEDMIOT_ID, NAZWA, count(*), count (STUDENT_ID), count(distinct STUDENT_ID)
from ZALICZENIA z, PRZEDMIOTY p
where z.PRZEDMIOT_ID = p.PRZEDMIOT_ID
group by p.PRZEDMIOT_ID, nazwa